﻿Shader "Unlit/Transparent Colored"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100

		Blend SrcAlpha OneMinusSrcAlpha
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment passThrough	
			

			#include "UnityCG.cginc"

			struct VertOut
	{
		float4 position : POSITION;
		float4 color : COLOR;
	};
	struct VertIn
	{
		float4 vertex : POSITION;
		float4 color : COLOR;
	};
			struct FragOut
			{
				float4 color : COLOR;
			};

			
			float4 _Color;
			
			VertOut vert (VertIn v)
			{
				VertOut o;
				o.position = UnityObjectToClipPos(v.vertex);
				o.color = _Color;
				
				return o;
			}
			
			
			FragOut passThrough(float4 color : COLOR)
			{
				FragOut output;
				output.color = _Color;
				return output;
			}
			ENDCG
		}
	}
}
