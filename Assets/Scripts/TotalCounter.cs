﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class TotalCounter : MonoBehaviour
	{
		[SerializeField]
		private LevelManager levelManager;
		[SerializeField]
		private Text label;
		[SerializeField]
		private string format = "{0}";

		private void Awake()
		{
			levelManager.BallsCount.OnChanged += OnBallsChanged;
			label.text = FormatString(levelManager.BallsCount.Value);
		}

		private void OnBallsChanged(int count)
		{
			label.text = FormatString(count);
		}


		private string FormatString(int count)
		{
			return string.Format(format, count);
		}
	}
}