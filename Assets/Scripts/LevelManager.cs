﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class LevelManager : MonoBehaviour
	{
		public SubscribingValue<int> Score = new SubscribingValue<int>();
		public SubscribingValue<int> LeftBalls = new SubscribingValue<int>();
		public SubscribingValue<int> BallsCount = new SubscribingValue<int>();
		public SubscribingValue<int> CurrentLevel = new SubscribingValue<int>();
		public Toggle.ToggleEvent OnPause = new Toggle.ToggleEvent();
		public Button.ButtonClickedEvent OnLevelInfo = new Button.ButtonClickedEvent();


		[SerializeField]
		private List<Level> levels;
		[SerializeField]
		private BallsSpawner spawner;
		[SerializeField]
		private ClickHandler clickHandler;

		private bool Paused;

		public int activeBalls;
		public int activeCursed;

		private void Awake()
		{
			spawner.OnHit += AddScore;
			spawner.OnHide += OnBallHide;
			clickHandler.OnHide += OnBallHide;
		}

		private void Start()
		{
			OnPause.Invoke(false);
			PrepareLevel();
		}

		private void Update()
		{
			if (Input.GetKeyUp(KeyCode.Space))
			{
				Paused = !Paused;
				Time.timeScale = Paused ? 0 : 1;
				clickHandler.TrackMouse = !Paused;
				OnPause.Invoke(Paused);
			}
		}

		private void OnBallHide()
		{
			activeCursed--;
			activeBalls--;
			if (activeCursed <= 0 || activeBalls <= 0)
			{
				if (LeftBalls == 0)
				{
					CurrentLevel.Value++;
					PrepareLevel();
				}
				else
				{
					PrepareLevel();
				}
			}
		}

		public void PrepareLevel()
		{
			activeCursed = 1;
			spawner.Clear();
			clickHandler.Clear();
			clickHandler.TrackMouse = false;
			if (CurrentLevel >= levels.Count) return;
			Score.Value = 0;
			activeBalls = levels[CurrentLevel].BallsCount;
			BallsCount.Value = activeBalls;
			activeBalls++;
			LeftBalls.Value = levels[CurrentLevel].MinCount;
			OnLevelInfo.Invoke();
		}

		public void StartLevel()
		{
			spawner.Spawn(activeBalls);
			clickHandler.TrackMouse = true;
		}

		private void AddScore()
		{
			Score.Value++;
			activeCursed++;
			LeftBalls.Value = LeftBalls > 0 ? LeftBalls - 1 : 0;
		}
	}
}