﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class LevelLabel : MonoBehaviour
	{
		[SerializeField]
		private LevelManager levelManager;
		[SerializeField]
		private Text label;
		[SerializeField]
		private string format = "{0}";

		private void Awake()
		{
			levelManager.CurrentLevel.OnChanged += OnLevelChanged;
			label.text = FormatString(levelManager.CurrentLevel.Value);
		}

		private void OnLevelChanged(int level)
		{
			label.text = FormatString(level);
		}

		private string FormatString(int level)
		{
			return string.Format(format, level + 1);
		}
	}
}