﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
	public class ClickHandler : MonoBehaviour
	{
		public event Action OnHide;
		public bool TrackMouse;

		[SerializeField]
		private Camera mainCamera;
		[SerializeField]
		private GrowController growPrefab;

		private GrowController initiator;
		private bool alreadyClicked;

		public void OnMouseUp()
		{
			if (alreadyClicked || !TrackMouse) return;

			alreadyClicked = true;
			var pos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
			pos.z = 0;
			initiator = Instantiate(growPrefab, pos, Quaternion.identity, transform);
			initiator.OnHide += OnBallHide;
		}

		private void OnBallHide(GrowController growController)
		{
			growController.OnHide -= OnBallHide;
			Debug.Log("Hide");
			if (OnHide != null)
				OnHide();
			
		}

		public void Clear()
		{
			alreadyClicked = false;
			if (initiator != null)
			{
				Destroy(initiator.gameObject);
				initiator.OnHide -= OnBallHide;
				initiator = null;
			}
		}
	}
}