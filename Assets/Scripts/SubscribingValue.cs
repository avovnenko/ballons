using System;

namespace Assets.Scripts
{
	public class SubscribingValue<T> where T : IEquatable<T>
	{
		private T _value;

		public event Action<T> OnChanged;

		public T Value
		{
			get
			{
				return _value;
			}
			set
			{
				if (!_value.Equals(value))
				{
					if (OnChanged != null)
						OnChanged(value);
					_value = value;
				}
			}
		}

		public static implicit operator T(SubscribingValue<T> obj)
		{
			return obj.Value;
		}
	}
}