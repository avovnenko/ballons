﻿using System;

namespace Assets.Scripts
{
	[Serializable]
	public class Level
	{
		public int BallsCount;
		public int MinCount;
	}
}