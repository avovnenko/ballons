﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using Random = UnityEngine.Random;

public class MoveController : MonoBehaviour
{
	public event Action<MoveController, GrowController> OnHit;

	private static Color[] Colors =
	{
		Color.green, 
		Color.black, 
		Color.blue,
		Color.cyan,  
		Color.grey,  
		Color.magenta,  
		Color.yellow,  
		Color.red,  
	};


	[SerializeField]
	private float speed;

	private Vector3 currentDirection;
	private Rigidbody _rigidbody;

	void Awake()
	{
		_rigidbody = GetComponent<Rigidbody>();
	}

	void Start()
	{
		var rotation = Quaternion.Euler(new Vector3(0, 0, (Random.value*50 + 15f)));
		rotation *= Quaternion.Euler(0, 0, Mathf.Floor(Random.value*4)*90);

		_rigidbody.velocity = rotation * (Vector3.right * speed);

		var color = Colors[Random.Range(0, Colors.Length)];
		color.a = 0.75f;
		gameObject.GetComponent<Renderer>().material.color = color;
	}


	public void Stop(GrowController growController)
	{
		_rigidbody.velocity = Vector3.zero;
		_rigidbody.isKinematic = true;
		if (OnHit != null) OnHit(this, this.GetComponent<GrowController>());
	}
}
