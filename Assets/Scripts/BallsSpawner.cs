﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
	public class BallsSpawner : MonoBehaviour
	{
		[SerializeField]
		private int startCount;
		[SerializeField]
		private Vector2 fieldSize;
		[SerializeField]
		private MoveController ballPrefab;

		public event Action OnHit;
		public event Action OnHide;


		private List<MoveController> balls = new List<MoveController>();

		public void Spawn(int count)
		{
			for (int i = 0; i < count; i++)
			{
				MoveController ball = Instantiate(ballPrefab, new Vector3((Random.value* 2 - 1) * fieldSize.x, (Random.value * 2 - 1) * fieldSize.y), Quaternion.identity,
					transform);
				ball.OnHit += OnBallHitted;
				balls.Add(ball);
			}
		}

		public void Clear()
		{
			foreach (var ball in balls)
			{
				if (ball)
				{
					Destroy(ball.gameObject);
					ball.OnHit -= OnBallHitted;
				}
			}
			balls.Clear();
		}

		private void OnBallHitted(MoveController ball, GrowController growController)
		{
			ball.OnHit -= OnBallHitted;
			if (OnHit != null)
				OnHit();

			
			growController.OnHide += OnBallHide;
		}

		private void OnBallHide(GrowController growController)
		{
			growController.OnHide -= OnBallHide;
			Debug.Log("Hide spawner");
			if (OnHide != null)
				OnHide();
		}
	}
}