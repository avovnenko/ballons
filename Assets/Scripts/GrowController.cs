﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
	public class GrowController : MonoBehaviour
	{
		public event Action<GrowController> OnHide;

		private float growTime = 3;
		private float decreaseTime = 0.5f;
		private float maxRadius = 2;
		private Transform _transform;
		private IEnumerator growCoroutine;

		private void Awake()
		{
			_transform = transform;
		}

		void Start()
		{
			StartCoroutine(growCoroutine = GetGrowCoroutine());
		}

		private IEnumerator GetGrowCoroutine()
		{
			var leftTime = growTime;
			var startScale = _transform.localScale;
			while (true)
			{
				leftTime -= Time.deltaTime;
				if (leftTime < 0)
					break;
				_transform.localScale = Vector3.Lerp(startScale, maxRadius * Vector3.one, growTime - leftTime);
				
				yield return null;
			}

			leftTime = decreaseTime;
			while (true)
			{
				leftTime -= Time.deltaTime;
				_transform.localScale = Vector3.Lerp(Vector3.one * maxRadius, Vector3.zero, decreaseTime - leftTime);
				if (leftTime < 0)
					break;
				yield return null;
			}

			growCoroutine = null;
			if (OnHide != null)
				OnHide(this);

			Destroy(gameObject);

		}

		public void OnCollisionEnter(Collision collision)
		{
			if (growCoroutine == null) return;

			var ball = collision.gameObject;
			ball.layer = gameObject.layer;
			ball.AddComponent<GrowController>();
			ball.GetComponent<MoveController>().Stop(this);

			
		}
	}
}